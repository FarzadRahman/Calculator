package com.example.calculator.calculator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView log;
    SharedPreferences sf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

    }

    @Override
    protected void onStart() {
        super.onStart();

        log=(TextView)findViewById(R.id.log);
        sf=getSharedPreferences("calLog",this.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        log.setText(sf.getString("log","").toString());


    }

    public void back(View v){
        onBackPressed();


    }

    public void clear(View v){

        log.setText("");
        SharedPreferences.Editor editor=sf.edit();
        editor.putString("log","");
        editor.apply();
    }


}
